//===- StencilDialect.h - MLIR Stencil Dialect ------------------*- C++ -*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file declares the stencil dialect in MLIR.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_DIALECT_STENCIL_STENCIL_STENCILDIALECT_H
#define MLIR_DIALECT_STENCIL_STENCIL_STENCILDIALECT_H

#include "mlir/IR/Dialect.h"

namespace mlir {
namespace stencil {

class StencilDialect : public Dialect {
public:
  explicit StencilDialect(MLIRContext *context);

  static StringRef getDialectNamespace() { return "stencil"; }

  /// Checks if the given type is valid in the Stencil dialect
  static bool isValidType(Type type);
  /// Checks if the given type is a valid scalar type in the Stencil dialect
  static bool isValidStencilScalarType(Type type);

  /// Parses a type registered to this dialect
  Type parseType(StringRef spec, Location loc) const override;

  /// Prints a type registered to this dialect
  void printType(Type type, raw_ostream &os) const override;

  /// Provides a hook for materializing a constant to this dialect
  Operation *materializeConstant(OpBuilder &builder, Attribute value, Type type,
                                 Location loc) override;
};

} // namespace stencil
} // namespace mlir

#endif // MLIR_DIALECT_STENCIL_STENCIL_STENCILDIALECT_H
