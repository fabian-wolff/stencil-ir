//===- IIRDialect.h - MLIR stencil IIR dialect ------------------*- C++ -*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file declares the stencil IIR dialect in MLIR.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_DIALECT_STENCIL_IIR_IIRDIALECT_H
#define MLIR_DIALECT_STENCIL_IIR_IIRDIALECT_H

#include "mlir/IR/Dialect.h"

namespace mlir {
namespace iir {

class IIRDialect : public Dialect {
public:
  explicit IIRDialect(MLIRContext *context);

  static StringRef getDialectNamespace() { return "iir"; }

  /// Parses a type registered to this dialect
  Type parseType(StringRef spec, Location loc) const override;

  /// Prints a type registered to this dialect
  void printType(Type type, raw_ostream &os) const override;
};

} // namespace iir
} // namespace mlir

#endif // MLIR_DIALECT_STENCIL_IIR_IIRDIALECT_H
