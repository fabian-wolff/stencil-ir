//===- IIRStructureOps.td - MLIR stencil IIR Structure Ops -*- tablegen -*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file contains ops for defining the IIR structure: stencils, stages,
// do methods, statement access pairs, etc.
//
//===----------------------------------------------------------------------===//

#ifdef IIR_STRUCTURE_OPS
#else
#define IIR_STRUCTURE_OPS

#ifdef IIR_BASE
#else
include "mlir/Dialect/Stencil/IIR/IIRBase.td"
#endif // IIR_BASE

def IIR_IIROp : IIR_Op<"iir",
                       [SingleBlockImplicitTerminator<"IIREndOp">]> {
    let arguments = (ins);
    let results = (outs);

    let regions = (region SizedRegion<1>:$stencils, SizedRegion<1>:$idToStencilCall,
                          SizedRegion<1>:$controlFlow);

    let builders = [OpBuilder<"Builder *, OperationState &state">];
    let parser = [{ return parseRegionOp<iir::IIROp>(parser, result, 3); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getStencils() {
            return this->getOperation()->getRegion(0).front();
        }
        Block &getIDToStencilCall() {
            return this->getOperation()->getRegion(1).front();
        }
        Block &getControlFlow() {
            return this->getOperation()->getRegion(2).front();
        }
    }];
}

def IIR_IIREndOp : IIR_Op<"_iir_end", [InIIRScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_StencilOp : IIR_Op<"stencil",
                           [SingleBlockImplicitTerminator<"StencilEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         OptionalAttr<I64ArrayAttr>:$stencilAttributes);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "ArrayRef<iir::StencilAttribute>">];
    let parser = [{ return parseRegionOp<iir::StencilOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_StencilEndOp : IIR_Op<"_stencil_end", [InStencilScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_MultiStageOp : IIR_Op<"multi_stage",
                              [SingleBlockImplicitTerminator<"MultiStageEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         IIR_LoopOrderAttr:$loopOrder);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "iir::LoopOrder">];
    let parser = [{ return parseRegionOp<iir::MultiStageOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_MultiStageEndOp : IIR_Op<"_multi_stage_end",
                                 [InMultiStageScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_StageOp : IIR_Op<"stage",
                         [SingleBlockImplicitTerminator<"StageEndOp">]> {
    let arguments = (ins I64Attr:$id);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t">];
    let parser = [{ return parseRegionOp<iir::StageOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_StageEndOp : IIR_Op<"_stage_end",
                            [InStageScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_DoMethodOp : IIR_Op<"do_method",
                            [SingleBlockImplicitTerminator<"DoMethodEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         DictionaryAttr:$interval);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "ArrayRef<int64_t>">];
    let parser = [{ return parseRegionOp<iir::DoMethodOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_DoMethodEndOp : IIR_Op<"_do_method_end",
                               [InDoMethodScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_StatementAccessPairOp : IIR_Op<"statement_access_pair",
                                       [SingleBlockImplicitTerminator<"StatementAccessPairEndOp">]> {
    let arguments = (ins  OptionalAttr<I64ArrayAttr>:$writeAccesses,
                          OptionalAttr<I64ArrayAttr>:$readAccesses);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, ArrayRef<int64_t>,"
                              "ArrayRef<int64_t>">];
    let parser = [{ return parseRegionOp<iir::StatementAccessPairOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_StatementAccessPairEndOp : IIR_Op<"_statement_access_pair_end",
                                          [InStatementAccessPairScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_IDToStencilCallEntryOp : IIR_Op<"id_to_stencil_call_entry",
                                        [SingleBlockImplicitTerminator<"IDToStencilCallEntryEndOp">]> {
    let arguments = (ins I64Attr:$id);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t">];
    let parser = [{ return parseRegionOp<iir::IDToStencilCallEntryOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_IDToStencilCallEntryEndOp : IIR_Op<"_id_to_stencil_call_entry_end",
                                           [InIDToStencilCallEntryScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

#endif // IIR_STRUCTURE_OPS
