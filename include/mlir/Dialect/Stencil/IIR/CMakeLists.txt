set(LLVM_TARGET_DEFINITIONS IIROps.td)
mlir_tablegen(IIROps.h.inc -gen-op-decls)
mlir_tablegen(IIROps.cpp.inc -gen-op-defs)
add_public_tablegen_target(MLIRIIROpsIncGen)

set(LLVM_TARGET_DEFINITIONS IIRBase.td)
mlir_tablegen(IIREnums.h.inc -gen-enum-decls)
mlir_tablegen(IIREnums.cpp.inc -gen-enum-defs)
add_public_tablegen_target(MLIRIIREnumsIncGen)
