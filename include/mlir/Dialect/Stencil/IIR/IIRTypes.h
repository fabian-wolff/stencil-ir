//===- IIRTypes.h - MLIR IIR Types ------------------------------*- C++ -*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file declares the types in the IIR dialect.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_DIALECT_STENCIL_IIR_IIRTYPES_H
#define MLIR_DIALECT_STENCIL_IIR_IIRTYPES_H

#include "mlir/IR/StandardTypes.h"
#include "mlir/IR/TypeSupport.h"
#include "mlir/IR/Types.h"

#include "mlir/Dialect/Stencil/IIR/IIREnums.h.inc"

namespace mlir {
namespace iir {}
} // namespace mlir

#endif // MLIR_DIALECT_STENCIL_IIR_IIRTYPES_H
