#ifndef IIR_EXPR_OPS
#define IIR_EXPR_OPS

#ifdef IIR_BASE
#else
include "mlir/Dialect/Stencil/IIR/IIRBase.td"
#endif // IIR_BASE

def IIR_AssignmentExprOp : IIR_Op<"assignment_expr",
                                 [SingleBlockImplicitTerminator<"AssignmentExprEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         IIR_AssignmentOperatorAttr:$op);
    let results = (outs);

    let regions = (region SizedRegion<1>:$left, SizedRegion<1>:$right);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "iir::AssignmentOperator op">];
    let parser = [{ return parseRegionOp<iir::AssignmentExprOp>(parser, result, 2); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getLeft() {
            return this->getOperation()->getRegion(0).front();
        }
        Block &getRight() {
            return this->getOperation()->getRegion(1).front();
        }
    }];
}

def IIR_AssignmentExprEndOp : IIR_Op<"_assignment_expr_end",
                                    [InAssignmentExprScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_FieldAccessExprOp : IIR_Op<"field_access_expr"> {
    let arguments = (ins I64Attr:$id,
                         StrAttr:$name,
                         I64ArrayAttr:$offset,
                         I64ArrayAttr:$argumentMap,
                         I64ArrayAttr:$argumentOffset,
                         BoolAttr:$negateOffset);
    let results = (outs);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "StringRef, ArrayRef<int64_t>,"
                              "ArrayRef<int64_t>, ArrayRef<int64_t>,"
                              "bool">];

    let skipDefaultBuilders = 1;
}

def IIR_BinaryOperatorOp : IIR_Op<"binary_operator",
                                  [SingleBlockImplicitTerminator<"BinaryOperatorEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         IIR_BinaryOperatorAttr:$op);
    let results = (outs);

    let regions = (region SizedRegion<1>:$left, SizedRegion<1>:$right);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "iir::BinaryOperator op">];
    let parser = [{ return parseRegionOp<iir::BinaryOperatorOp>(parser, result, 2); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getLeft() {
            return this->getOperation()->getRegion(0).front();
        }
        Block &getRight() {
            return this->getOperation()->getRegion(1).front();
        }
    }];
}

def IIR_BinaryOperatorEndOp : IIR_Op<"_binary_operator_end",
                                     [InBinaryOperatorScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_VarAccessExprOp : IIR_Op<"var_access_expr",
                                 [SingleBlockImplicitTerminator<"VarAccessExprEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         StrAttr:$name,
                         BoolAttr:$isExternal);
    let results = (outs);

    let regions = (region SizedRegion<1>:$index);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "StringRef, bool">];
    let parser = [{ return parseRegionOp<iir::VarAccessExprOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getIndex() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_VarAccessExprEndOp : IIR_Op<"_var_access_expr_end",
                                    [InVarAccessExprScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_LiteralAccessExprOp : IIR_Op<"literal_access_expr"> {
    let arguments = (ins I64Attr:$id,
                         StrAttr:$value,
                         IIR_BuiltinTypeAttr:$builtinType);
    let results = (outs);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "StringRef, iir::BuiltinType">];

    let skipDefaultBuilders = 1;
}

def IIR_TernaryOperatorOp : IIR_Op<"ternary_operator",
                                   [SingleBlockImplicitTerminator<"TernaryOperatorEndOp">]> {
    let arguments = (ins I64Attr:$id);
    let results = (outs);

    let regions = (region SizedRegion<1>:$cond, SizedRegion<1>:$left, SizedRegion<1>:$right);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t">];
    let parser = [{ return parseRegionOp<iir::TernaryOperatorOp>(parser, result, 3); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getCond() {
            return this->getOperation()->getRegion(0).front();
        }
        Block &getLeft() {
            return this->getOperation()->getRegion(1).front();
        }
        Block &getRight() {
            return this->getOperation()->getRegion(2).front();
        }
    }];
}

def IIR_TernaryOperatorEndOp : IIR_Op<"_ternary_operator_end",
                                      [InTernaryOperatorScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_UnaryOperatorOp : IIR_Op<"unary_operator",
                                 [SingleBlockImplicitTerminator<"UnaryOperatorEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         IIR_UnaryOperatorAttr:$op);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "iir::UnaryOperator">];
    let parser = [{ return parseRegionOp<iir::UnaryOperatorOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_UnaryOperatorEndOp : IIR_Op<"_unary_operator_end",
                                    [InUnaryOperatorScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_FunCallOp : IIR_Op<"function_call",
                           [SingleBlockImplicitTerminator<"FunCallEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         StrAttr:$callee);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "StringRef">];
    let parser = [{ return parseRegionOp<iir::FunCallOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_FunCallEndOp : IIR_Op<"_function_call_end",
                              [InFunCallScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_StencilFunCallOp : IIR_Op<"stencil_function_call",
                                  [SingleBlockImplicitTerminator<"StencilFunCallEndOp">]> {
    let arguments = (ins I64Attr:$id,
                         StrAttr:$callee);
    let results = (outs);

    let regions = (region SizedRegion<1>:$body);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "StringRef">];
    let parser = [{ return parseRegionOp<iir::StencilFunCallOp>(parser, result); }];
    let printer = [{ printRegionOp(getOperation(), p); }];
    let verifier = [{ return verifyIIRBody(*this); }];

    // We need to ensure the block inside the region is properly terminated;
    // the auto-generated builders do not guarantee that.
    let skipDefaultBuilders = 1;

    let extraClassDeclaration = [{
        Block &getBody() {
            return this->getOperation()->getRegion(0).front();
        }
    }];
}

def IIR_StencilFunCallEndOp : IIR_Op<"_stencil_function_call_end",
                              [InStencilFunCallScope, Terminator]> {
    let arguments = (ins);
    let results = (outs);

    let parser = [{ return parseNoIOOp(parser, result); }];
    let printer = [{ printNoIOOp(getOperation(), p); }];

    let verifier = [{ return success(); }];
}

def IIR_StencilFunArgExprOp : IIR_Op<"stencil_function_argument_expr"> {
    let arguments = (ins I64Attr:$id,
                         I64Attr:$index,
                         I64Attr:$offset,
                         OptionalAttr<IIR_DimensionAttr>:$dimension);
    let results = (outs);

    let builders = [OpBuilder<"Builder *, OperationState &, int64_t,"
                              "int64_t, int64_t, iir::Dimension">,
                    OpBuilder<"Builder *, OperationState &, int64_t,"
                              "int64_t, int64_t">];

    let skipDefaultBuilders = 1;
}

def IIR_StencilCallOp : IIR_Op<"stencil_call"> {
    let arguments = (ins StrAttr:$callee,
                         ArrayAttr:$arguments);
    let results = (outs);

    let builders = [OpBuilder<"Builder *, OperationState &, StringRef,"
                              "ArrayRef<StringRef>">];

    let skipDefaultBuilders = 1;
}

#endif // IIR_EXPR_OPS
