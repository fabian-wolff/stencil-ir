//===- ConvertIIRToStencil.h - Convert to Stencil dialect -------*- C++ -*-===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// Provides type converters and patterns to convert from IIR types/ops to
// Stencil types and operations.
//
//===----------------------------------------------------------------------===//

#ifndef MLIR_CONVERSION_IIRTOSTENCIL_CONVERTIIRTOSTENCIL_H
#define MLIR_CONVERSION_IIRTOSTENCIL_CONVERTIIRTOSTENCIL_H

#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"
#include "mlir/Support/StringExtras.h"
#include "mlir/Transforms/DialectConversion.h"

namespace mlir {

/// Type conversion from IIR types to Stencil types.
class StencilBasicTypeConverter : public TypeConverter {
public:
  /// Converts types to Stencil supported types.
  Type convertType(Type t) override;
};

using StencilTypeConverter = StencilBasicTypeConverter;

/// Base class to define a conversion pattern to translate Ops to the Stencil
/// dialect.
template <typename OpTy>
class StencilOpLowering : public ConversionPattern {
public:
  StencilOpLowering(MLIRContext *context, StencilTypeConverter &typeConverter)
      : ConversionPattern(OpTy::getOperationName(), 1, context),
        typeConverter(typeConverter) {}

protected:
  StencilTypeConverter &typeConverter;
};

/// Appends to a pattern list additional patterns for translating IIR ops to
/// Stencil ops.
void populateIIRToStencilPatterns(MLIRContext *context,
                                  OwningRewritePatternList &patterns);

} // namespace mlir

#endif // MLIR_CONVERSION_IIRTOSTENCIL_CONVERTIIRTOSTENCIL_H
