//===- ConvertIIRToStencil.cpp - IIR to Stencil dialect conversion --------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file implements a pass to convert the MLIR IIR dialect into the Stencil
// dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/Conversion/IIRToStencil/ConvertIIRToStencil.h"
#include "mlir/Dialect/Stencil/IIR/IIROps.h"
#include "mlir/Dialect/Stencil/Stencil/StencilDialect.h"
#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"

#include <stack>
#include <string>
#include <utility>

using namespace mlir;

//===----------------------------------------------------------------------===//
// Type Conversion
//===----------------------------------------------------------------------===//

Type StencilBasicTypeConverter::convertType(Type t) {
  if (stencil::StencilDialect::isValidType(t))
    return t;
  return Type();
}

//===----------------------------------------------------------------------===//
// Operation conversion
//===----------------------------------------------------------------------===//

namespace {

using ValueStack = std::stack<Value *>;

void convert(iir::LiteralAccessExprOp literalAccessExprOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = literalAccessExprOp.getOperation();
  auto builtinType = literalAccessExprOp.builtinType();
  auto value = literalAccessExprOp.value();

  Type retType;
  Attribute valueAttr;
  switch (builtinType) {
  case iir::BuiltinType::Boolean: {
    retType = builder.getI1Type();
    if (value == "false") {
      valueAttr = builder.getBoolAttr(false);
    } else if (value == "true") {
      valueAttr = builder.getBoolAttr(true);
    } else {
      llvm_unreachable(
          (std::string("Unknown boolean value ") + value.str()).c_str());
    }
    break;
  }
  case iir::BuiltinType::Integer: {
    int64_t ival = 0;
    retType = builder.getIntegerType(64);
    value.getAsInteger(10, ival);
    valueAttr = builder.getI64IntegerAttr(ival);
    break;
  }
  case iir::BuiltinType::Float: {
    double fval = 0.0;
    retType = builder.getF64Type();
    value.getAsDouble(fval);
    valueAttr = builder.getF64FloatAttr(fval);
    break;
  }
  case iir::BuiltinType::Auto:
    llvm_unreachable("Auto type not supported");
  case iir::BuiltinType::Invalid:
  default:
    llvm_unreachable("Unknown IIR builtin type");
  }

  auto constantOp =
      builder.create<stencil::ConstantOp>(op->getLoc(), retType, valueAttr);

  stack.push(constantOp.getResult());
}

void convert(iir::FieldAccessExprOp fieldAccessExprOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = fieldAccessExprOp.getOperation();
  auto name = fieldAccessExprOp.name();
  auto offset = fieldAccessExprOp.offset();

  auto stencilOp = op->getParentOfType<stencil::StencilOp>();
  assert(
      stencilOp &&
      "iir::FieldAccessOp should have a stencil::StencilOp parent operation");
  auto argNamesAttr = stencilOp.getAttr("argNames");
  assert(argNamesAttr && "stencilOp should have an 'argNames' attribute");
  auto argNames = argNamesAttr.cast<ArrayAttr>().getValue();

  // Check if the accessed field is an API field
  int fieldIndex = -1;
  for (int i = 0, e = argNames.size(); i < e; ++i) {
    auto argName = argNames[i].cast<StringAttr>().getValue();
    if (argName == name) {
      fieldIndex = i;
      break;
    }
  }

  Value *field = nullptr;
  if (fieldIndex >= 0)
    field = stencilOp.getBody().getArgument(fieldIndex);
  else
    // FIXME: We should register non-API fields only once
    field = builder.create<stencil::FieldOp>(op->getLoc(), name).getResult();

  auto fieldAccessOp =
      builder.create<stencil::FieldAccessOp>(op->getLoc(), field, offset);

  stack.push(fieldAccessOp.getResult());
}

void convert(iir::VarAccessExprOp varAccessExprOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = varAccessExprOp.getOperation();
  auto name = varAccessExprOp.name();

  auto varOp = builder.create<stencil::VarOp>(op->getLoc(), name);
  auto varAccessOp =
      builder.create<stencil::VarAccessOp>(op->getLoc(), varOp.getResult());

  stack.push(varAccessOp.getResult());
}

void convert(iir::BinaryOperatorOp binaryOperatorOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = binaryOperatorOp.getOperation();
  auto binaryOperator = binaryOperatorOp.op();

  assert(stack.size() >= 2 &&
         "binary operator expects two operands on the stack");
  auto rhs = stack.top();
  stack.pop();
  auto lhs = stack.top();
  stack.pop();

  if (rhs->getType().isa<stencil::PointerType>()) {
    rhs = builder.create<stencil::GetValueOp>(op->getLoc(), rhs);
  }
  if (lhs->getType().isa<stencil::PointerType>()) {
    lhs = builder.create<stencil::GetValueOp>(op->getLoc(), lhs);
  }

  Value *result;
  switch (binaryOperator) {
  case iir::BinaryOperator::Plus:
    result = builder.create<stencil::AddOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  case iir::BinaryOperator::Minus:
    result = builder.create<stencil::SubOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  case iir::BinaryOperator::Times:
    result = builder.create<stencil::MulOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  case iir::BinaryOperator::Divide:
    result = builder.create<stencil::DivOp>(op->getLoc(), lhs, rhs).getResult();
    break;
  default:
    llvm_unreachable((std::string("Unsupported binary operator") +
                      iir::stringifyBinaryOperator(binaryOperator).str())
                         .c_str());
  }

  stack.push(result);
}

void convert(iir::UnaryOperatorOp unaryOperatorOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = unaryOperatorOp.getOperation();
  auto unaryOperator = unaryOperatorOp.op();

  assert(stack.size() >= 1 &&
         "unary operator expects one operand on the stack");

  auto arg = stack.top();
  stack.pop();

  if (arg->getType().isa<stencil::PointerType>()) {
    arg = builder.create<stencil::GetValueOp>(op->getLoc(), arg);
  }

  Value *result = nullptr;
  switch (unaryOperator) {
  case iir::UnaryOperator::UnaryMinus:
    result = builder.create<stencil::ConstantOp>(
        op->getLoc(), arg->getType(), builder.getZeroAttr(arg->getType()));
    result = builder.create<stencil::SubOp>(op->getLoc(), result, arg);
    break;

  default:
    llvm_unreachable((std::string("Unsupported unary operator") +
                      iir::stringifyUnaryOperator(unaryOperator).str())
                         .c_str());
  }

  stack.push(result);
}

void convert(iir::AssignmentExprOp assignmentExprOp, OpBuilder &builder,
             ValueStack &stack) {
  auto op = assignmentExprOp.getOperation();

  assert(stack.size() >= 2 &&
         "write operation expects two operands on the stack");
  auto value = stack.top();
  stack.pop();

  if(value->getType().isa<stencil::PointerType>())
    value = builder.create<stencil::GetValueOp>(op->getLoc(), value);

  auto target = stack.top();
  stack.pop();
  assert(target->getType().isa<stencil::PointerType>() &&
         "write target should be a pointer");

  switch (assignmentExprOp.op()) {
  case iir::AssignmentOperator::Equal:
    break;
  case iir::AssignmentOperator::PlusEqual: {
    auto getValueOp = builder.create<stencil::GetValueOp>(op->getLoc(), target);
    auto addOp = builder.create<stencil::AddOp>(op->getLoc(),
                                                getValueOp.getResult(), value);
    value = addOp.getResult();
  } break;
  case iir::AssignmentOperator::MinusEqual: {
    auto getValueOp = builder.create<stencil::GetValueOp>(op->getLoc(), target);
    auto subOp = builder.create<stencil::SubOp>(op->getLoc(),
                                                getValueOp.getResult(), value);
    value = subOp.getResult();
  } break;
  case iir::AssignmentOperator::TimesEqual: {
    auto getValueOp = builder.create<stencil::GetValueOp>(op->getLoc(), target);
    auto mulOp = builder.create<stencil::MulOp>(op->getLoc(),
                                                getValueOp.getResult(), value);
    value = mulOp.getResult();
  } break;
  case iir::AssignmentOperator::DivideEqual: {
    auto getValueOp = builder.create<stencil::GetValueOp>(op->getLoc(), target);
    auto divOp = builder.create<stencil::DivOp>(op->getLoc(),
                                                getValueOp.getResult(), value);
    value = divOp.getResult();
  } break;
  default:
    llvm_unreachable(
        (std::string("unsupported assignment operator ") +
         iir::stringifyAssignmentOperator(assignmentExprOp.op()).str())
            .c_str());
  }

  builder.create<stencil::WriteOp>(op->getLoc(), target, value);
}

void convert(iir::StencilOp iirStencilOp, OpBuilder &builder,
             const SmallVectorImpl<Type> &apiFieldTypes,
             const SmallVectorImpl<StringRef> &apiFieldNames) {
  auto op = iirStencilOp.getOperation();

  SmallVector<stencil::StencilAttribute, 3> stencilAttributes;
  if (iirStencilOp.getAttr("mergeDoMethods"))
    stencilAttributes.push_back(stencil::StencilAttribute::MergeDoMethods);
  if (iirStencilOp.getAttr("mergeStages"))
    stencilAttributes.push_back(stencil::StencilAttribute::MergeStages);
  if (iirStencilOp.getAttr("mergeTemporaries"))
    stencilAttributes.push_back(stencil::StencilAttribute::MergeTemporaries);
  if (iirStencilOp.getAttr("noCodeGen"))
    stencilAttributes.push_back(stencil::StencilAttribute::NoCodeGen);
  if (iirStencilOp.getAttr("useKCaches"))
    stencilAttributes.push_back(stencil::StencilAttribute::UseKCaches);

  auto stencilOp =
      builder.create<stencil::StencilOp>(op->getLoc(), stencilAttributes);
  stencilOp.getOperation()->getRegion(0).takeBody(op->getRegion(0));
  stencilOp.getBody().addArguments(apiFieldTypes);

  stencilOp.setAttr("argNames", builder.getStrArrayAttr(apiFieldNames));
}

template <typename IIRTerminatorOp, typename StencilTerminatorOp>
class TerminatorOpConversion final : public ConversionPattern {
public:
  explicit TerminatorOpConversion(MLIRContext *context)
      : ConversionPattern(IIRTerminatorOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOpWithNewOp<StencilTerminatorOp>(operation);

    return matchSuccess();
  }
};

class IIROpConversion final : public ConversionPattern {
public:
  explicit IIROpConversion(MLIRContext *context)
      : ConversionPattern(iir::IIROp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    SmallVector<StringRef, 3> apiFieldNames;
    auto apiFieldIDsAttr = operation->getAttr("apiFieldIDs");
    assert(apiFieldIDsAttr &&
           "iir::IIROp should have an 'apiFieldIDs' attribute");
    auto apiFieldIDs = apiFieldIDsAttr.cast<ArrayAttr>();
    auto accessIDToNameAttr = operation->getAttr("accessIDToName");
    assert(accessIDToNameAttr &&
           "iir::IIROp should have an 'accessIDToName' attribute");
    auto accessIDToName = accessIDToNameAttr.cast<DictionaryAttr>();
    for (const auto &apiFieldIDAttr : apiFieldIDs) {
      auto apiFieldID = apiFieldIDAttr.cast<IntegerAttr>().getInt();
      auto apiFieldNameAttr =
          accessIDToName.get(std::string("_") + std::to_string(apiFieldID));
      assert(apiFieldNameAttr && "unknown API field");
      auto apiFieldName = apiFieldNameAttr.cast<StringAttr>().getValue();
      apiFieldNames.push_back(apiFieldName);
    }

    auto iirOp = rewriter.create<stencil::IIROp>(operation->getLoc());
    iirOp.getOperation()->getRegion(0).takeBody(operation->getRegion(0));

    OpBuilder builder(&iirOp.getBody().front());

    SmallVector<Type, 3> apiFieldTypes(
        apiFieldNames.size(), stencil::FieldType::get(rewriter.getF64Type()));

    iirOp.getOperation()->walk([&](iir::StencilOp stencilOp) {
      convert(stencilOp, builder, apiFieldTypes, apiFieldNames);
    });

    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class StencilOpConversion final : public ConversionPattern {
public:
  explicit StencilOpConversion(MLIRContext *context)
      : ConversionPattern(iir::StencilOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class MultiStageOpConversion final : public ConversionPattern {
public:
  explicit MultiStageOpConversion(MLIRContext *context)
      : ConversionPattern(iir::MultiStageOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto loopOrder = cast<iir::MultiStageOp>(operation).loopOrder();
    auto multiStageOp = rewriter.create<stencil::MultiStageOp>(
        operation->getLoc(),
        *stencil::symbolizeLoopOrder(static_cast<int64_t>(loopOrder)));
    multiStageOp.getOperation()->getRegion(0).takeBody(operation->getRegion(0));

    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class StageOpConversion final : public ConversionPattern {
public:
  explicit StageOpConversion(MLIRContext *context)
      : ConversionPattern(iir::StageOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto stageOp = rewriter.create<stencil::StageOp>(operation->getLoc());
    stageOp.getOperation()->getRegion(0).takeBody(operation->getRegion(0));

    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class DoMethodOpConversion final : public ConversionPattern {
public:
  explicit DoMethodOpConversion(MLIRContext *context)
      : ConversionPattern(iir::DoMethodOp::getOperationName(), 1, context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    auto iirDoMethodOp = cast<iir::DoMethodOp>(operation);
    int64_t lowerLevel =
        iirDoMethodOp.interval().get("lowerLevel").cast<IntegerAttr>().getInt();
    int64_t upperLevel =
        iirDoMethodOp.interval().get("upperLevel").cast<IntegerAttr>().getInt();
    int64_t lowerOffset = iirDoMethodOp.interval()
                              .get("lowerOffset")
                              .cast<IntegerAttr>()
                              .getInt();
    int64_t upperOffset = iirDoMethodOp.interval()
                              .get("upperOffset")
                              .cast<IntegerAttr>()
                              .getInt();

    auto doMethodOp = rewriter.create<stencil::DoMethodOp>(
        operation->getLoc(),
        llvm::makeArrayRef({lowerLevel, upperLevel, lowerOffset, upperOffset}));
    doMethodOp.getOperation()->getRegion(0).takeBody(operation->getRegion(0));

    OpBuilder builder(&doMethodOp.getBody().front());
    ValueStack stack;

    doMethodOp.getOperation()->walk([&builder, &stack](Operation *op) {
      if (auto literalAccessExprOp = dyn_cast<iir::LiteralAccessExprOp>(op))
        convert(literalAccessExprOp, builder, stack);
      else if (auto fieldAccessExprOp = dyn_cast<iir::FieldAccessExprOp>(op))
        convert(fieldAccessExprOp, builder, stack);
      else if (auto varAccessExprOp = dyn_cast<iir::VarAccessExprOp>(op))
        convert(varAccessExprOp, builder, stack);
      else if (auto binaryOperatorOp = dyn_cast<iir::BinaryOperatorOp>(op))
        convert(binaryOperatorOp, builder, stack);
      else if (auto unaryOperatorOp = dyn_cast<iir::UnaryOperatorOp>(op))
        convert(unaryOperatorOp, builder, stack);
      else if (auto assignmentExprOp = dyn_cast<iir::AssignmentExprOp>(op))
        convert(assignmentExprOp, builder, stack);
    });

    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

class StatementAccessPairOpConversion final : public ConversionPattern {
public:
  explicit StatementAccessPairOpConversion(MLIRContext *context)
      : ConversionPattern(iir::StatementAccessPairOp::getOperationName(), 1,
                          context) {}

  PatternMatchResult
  matchAndRewrite(Operation *operation, ArrayRef<Value *> operands,
                  ConversionPatternRewriter &rewriter) const override {
    rewriter.replaceOp(operation, llvm::None);

    return matchSuccess();
  }
};

} // namespace

namespace {
/// Import the IIR Ops to Stencil Patterns.
#include "IIRToStencil.cpp.inc"
} // namespace

namespace mlir {
void populateIIRToStencilPatterns(MLIRContext *context,
                                  OwningRewritePatternList &patterns) {
  populateWithGenerated(context, &patterns);
  patterns.insert<IIROpConversion, StencilOpConversion, MultiStageOpConversion,
                  StageOpConversion, DoMethodOpConversion,
                  StatementAccessPairOpConversion>(context);
  patterns.insert<
      TerminatorOpConversion<iir::IIREndOp, stencil::IIREndOp>,
      TerminatorOpConversion<iir::StencilEndOp, stencil::StencilEndOp>,
      TerminatorOpConversion<iir::MultiStageEndOp, stencil::MultiStageEndOp>,
      TerminatorOpConversion<iir::StageEndOp, stencil::StageEndOp>,
      TerminatorOpConversion<iir::DoMethodEndOp, stencil::DoMethodEndOp>>(
      context);
}
} // namespace mlir
