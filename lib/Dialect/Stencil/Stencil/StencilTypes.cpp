//===- StencilTypes.cpp - MLIR Stencil Types ------------------------------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file defines the types in the Stencil dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/Dialect/Stencil/Stencil/StencilTypes.h"

#include "llvm/ADT/StringSwitch.h"

// Pull in all enum utility function definitions
#include "mlir/Dialect/Stencil/Stencil/StencilEnums.cpp.inc"

using namespace mlir;
using namespace mlir::stencil;

//===----------------------------------------------------------------------===//
// FieldType
//===----------------------------------------------------------------------===//

struct stencil::detail::FieldTypeStorage : public TypeStorage {
  using KeyTy = Type;

  explicit FieldTypeStorage(const KeyTy &key) : elementType(key) {}

  static FieldTypeStorage *construct(TypeStorageAllocator &allocator,
                                     const KeyTy &key) {
    return new (allocator.allocate<FieldTypeStorage>()) FieldTypeStorage(key);
  }

  bool operator==(const KeyTy &key) const { return key == KeyTy(elementType); }

  Type elementType;
};

FieldType FieldType::get(Type elementType) {
  return Base::get(elementType.getContext(), TypeKind::Field, elementType);
}

Type FieldType::getElementType() const { return getImpl()->elementType; }

//===----------------------------------------------------------------------===//
// VarType
//===----------------------------------------------------------------------===//

struct stencil::detail::VarTypeStorage : public TypeStorage {
  using KeyTy = Type;

  explicit VarTypeStorage(const KeyTy &key) : contentType(key) {}

  static VarTypeStorage *construct(TypeStorageAllocator &allocator,
                                   const KeyTy &key) {
    return new (allocator.allocate<VarTypeStorage>()) VarTypeStorage(key);
  }

  bool operator==(const KeyTy &key) const { return key == KeyTy(contentType); }

  Type contentType;
};

VarType VarType::get(Type contentType) {
  return Base::get(contentType.getContext(), TypeKind::Var, contentType);
}

Type VarType::getContentType() const { return getImpl()->contentType; }

//===----------------------------------------------------------------------===//
// PointerType
//===----------------------------------------------------------------------===//

struct stencil::detail::PointerTypeStorage : public TypeStorage {
  using KeyTy = Type;

  explicit PointerTypeStorage(const KeyTy &key) : pointeeType(key) {}

  static PointerTypeStorage *construct(TypeStorageAllocator &allocator,
                                       const KeyTy &key) {
    return new (allocator.allocate<PointerTypeStorage>())
        PointerTypeStorage(key);
  }

  bool operator==(const KeyTy &key) const { return key == KeyTy(pointeeType); }

  Type pointeeType;
};

PointerType PointerType::get(Type pointeeType) {
  return Base::get(pointeeType.getContext(), TypeKind::Pointer, pointeeType);
}

Type PointerType::getPointeeType() const { return getImpl()->pointeeType; }
