//===- StencilDialect.cpp - MLIR Stencil dialect --------------------------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file defines the Stencil dialect in MLIR.
//
//===----------------------------------------------------------------------===//

#include "mlir/Dialect/Stencil/Stencil/StencilDialect.h"
#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"
#include "mlir/Dialect/Stencil/Stencil/StencilTypes.h"
#include "mlir/IR/Builders.h"

using namespace mlir;
using namespace mlir::stencil;

//===----------------------------------------------------------------------===//
// Stencil Dialect
//===----------------------------------------------------------------------===//

StencilDialect::StencilDialect(mlir::MLIRContext *context)
    : Dialect(getDialectNamespace(), context) {
  addTypes<FieldType, VarType, PointerType>();

  addOperations<
#define GET_OP_LIST
#include "mlir/Dialect/Stencil/Stencil/StencilOps.cpp.inc"
      >();

  allowsUnknownOperations();
}

//===----------------------------------------------------------------------===//
// Type Parsing
//===----------------------------------------------------------------------===//

static bool isValidStencilIntType(IntegerType type) {
  return llvm::is_contained(ArrayRef<unsigned>({1, 8, 16, 32, 64}),
                            type.getWidth());
}

bool StencilDialect::isValidStencilScalarType(Type type) {
  if (type.isa<FloatType>())
    return !type.isBF16();

  if (auto intType = type.dyn_cast<IntegerType>())
    return isValidStencilIntType(intType);

  return false;
}

bool StencilDialect::isValidType(Type type) {
  return isValidStencilScalarType(type) || type.isa<FieldType>() ||
         type.isa<VarType>() || type.isa<PointerType>();
}

Type StencilDialect::parseType(StringRef spec, Location loc) const {
  MLIRContext *context = getContext();
  if (spec.startswith("field:")) {
    SmallVector<StringRef, 2> ss;
    spec.split(ss, ':');
    if (ss.size() == 2) {
      StringRef typeStr = ss[1];
      if (typeStr == "f64")
        return FieldType::get(FloatType::getF64(context));
      else if (typeStr == "i64")
        return FieldType::get(IntegerType::get(64, context));
    }
  } else if (spec.startswith("ptr:")) {
    SmallVector<StringRef, 2> ss;
    spec.split(ss, ':');
    if (ss.size() == 2) {
      StringRef typeStr = ss[1];
      if (typeStr == "f64")
        return PointerType::get(FloatType::getF64(context));
      else if (typeStr == "i64")
        return PointerType::get(IntegerType::get(64, context));
    }
  } else if (spec.startswith("var:")) {
    SmallVector<StringRef, 2> ss;
    spec.split(ss, ':');
    if (ss.size() == 2) {
      StringRef typeStr = ss[1];
      if (typeStr == "f64")
        return VarType::get(FloatType::getF64(context));
      else if (typeStr == "i64")
        return VarType::get(IntegerType::get(64, context));
    }
  }

  return (emitError(loc, "unknown Stencil type: ") << spec, Type());
}

//===----------------------------------------------------------------------===//
// Type Printing
//===----------------------------------------------------------------------===//

static void print(FieldType ft, raw_ostream &os) {
  os << "field:" << ft.getElementType();
}

static void print(VarType vt, raw_ostream &os) {
  os << "var:" << vt.getContentType();
}

static void print(PointerType pt, raw_ostream &os) {
  os << "ptr:" << pt.getPointeeType();
}

void StencilDialect::printType(Type type, raw_ostream &os) const {
  switch (type.getKind()) {
  case TypeKind::Field:
    print(type.cast<FieldType>(), os);
    break;
  case TypeKind::Var:
    print(type.cast<VarType>(), os);
    break;
  case TypeKind::Pointer:
    print(type.cast<PointerType>(), os);
    break;
  default:
    llvm_unreachable("unhandled Stencil type");
  }
}

//===----------------------------------------------------------------------===//
// Constant
//===----------------------------------------------------------------------===//

Operation *StencilDialect::materializeConstant(OpBuilder &builder,
                                               Attribute value, Type type,
                                               Location loc) {
  if (!stencil::ConstantOp::isBuildableWith(type))
    return nullptr;

  return builder.create<stencil::ConstantOp>(loc, type, value);
}
