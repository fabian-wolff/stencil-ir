//===- StencilOps.cpp - MLIR Stencil operations ---------------------------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file defines the operations in the Stencil dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/Dialect/Stencil/Stencil/StencilOps.h"

#include "mlir/Dialect/Stencil/Stencil/StencilDialect.h"
#include "mlir/Dialect/Stencil/Stencil/StencilTypes.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Function.h"
#include "mlir/IR/OpImplementation.h"

using namespace mlir;

static constexpr const char kValueAttrName[] = "value";
static constexpr const char kNameAttrName[] = "name";
static constexpr const char kOffsetAttrName[] = "offset";
static constexpr const char kIntervalAttrName[] = "interval";
static constexpr const char kIntervalLowerLevelAttrName[] = "lowerLevel";
static constexpr const char kIntervalUpperLevelAttrName[] = "upperLevel";
static constexpr const char kIntervalLowerOffsetAttrName[] = "lowerOffset";
static constexpr const char kIntervalUpperOffsetAttrName[] = "upperOffset";
static constexpr const char kLoopOrderAttrName[] = "loopOrder";
static constexpr const char kMergeDoMethodsAttrName[] = "mergeDoMethods";
static constexpr const char kMergeStagesAttrName[] = "mergeStages";
static constexpr const char kMergeTemporariesAttrName[] = "mergeTemporaries";
static constexpr const char kNoCodeGenAttrName[] = "noCodeGen";
static constexpr const char kUseKCachesAttrName[] = "useKCaches";

//===----------------------------------------------------------------------===//
// Common utility functions
//===----------------------------------------------------------------------===//

template <typename Dst, typename Src>
inline Dst bitwiseCast(Src source) noexcept {
  Dst dest;
  static_assert(sizeof(source) == sizeof(dest),
                "bitwiseCast requires same source and destination bitwidth");
  std::memcpy(&dest, &source, sizeof(dest));
  return dest;
}

// Parses an op that has no inputs and no outputs.
static ParseResult parseNoIOOp(OpAsmParser &parser, OperationState &state) {
  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();
  return success();
}

// Prints an op that has no inputs and no outputs.
static void printNoIOOp(Operation *op, OpAsmPrinter &printer) {
  printer << op->getName();
  printer.printOptionalAttrDict(op->getAttrs());
}

static void printRegionOp(Operation *op, OpAsmPrinter &printer) {
  printer << op->getName();

  for (auto &region : op->getRegions()) {
    printer.printRegion(region, false, false);
  }

  if (!op->getAttrs().empty()) {
    printer << " attributes";
    printer.printOptionalAttrDict(op->getAttrs());
  }
}

template <typename StructureOp>
static ParseResult parseRegionOp(OpAsmParser &parser, OperationState &state,
                                 unsigned int nRegions = 1) {
  llvm::SmallVector<Region *, 2> regions;
  for (unsigned int i = 0; i < nRegions; ++i)
    regions.push_back(state.addRegion());

  for (auto &region : regions) {
    if (parser.parseRegion(*region, /*arguments=*/{}, /*argTypes=*/{}))
      return failure();
    StructureOp::ensureTerminator(*region, parser.getBuilder(), state.location);
  }

  if (succeeded(parser.parseOptionalKeyword("attributes"))) {
    if (parser.parseOptionalAttributeDict(state.attributes))
      return failure();
  }

  return success();
}

//===----------------------------------------------------------------------===//
// stencil.constant
//===----------------------------------------------------------------------===//

static ParseResult parseConstantOp(OpAsmParser &parser, OperationState &state) {
  Attribute value;
  if (parser.parseAttribute(value, kValueAttrName, state.attributes))
    return failure();

  Type type = value.getType();

  return parser.addTypesToList(type, state.types);
}

static void print(stencil::ConstantOp constantOp, OpAsmPrinter &printer) {
  printer << stencil::ConstantOp::getOperationName() << ' '
          << constantOp.value();
}

static LogicalResult verify(stencil::ConstantOp constantOp) {
  auto opType = constantOp.getType();
  auto value = constantOp.value();
  auto valueType = value.getType();

  // ODS already generates checks to make sure the result type is valid. We just
  // need to additionally check that the value's attribute type is consistent
  // with the result type.
  switch (value.getKind()) {
  case StandardAttributes::Bool:
  case StandardAttributes::Integer:
  case StandardAttributes::Float:
    if (valueType != opType)
      return constantOp.emitOpError("result type (")
             << opType << ") does not match value type (" << valueType << ")";
    return success();
  default:
    return constantOp.emitOpError("cannot have a value of type ") << valueType;
  }
}

OpFoldResult stencil::ConstantOp::fold(ArrayRef<Attribute> operands) {
  assert(operands.empty() && "stencil.constant has no operands");
  return value();
}

bool stencil::ConstantOp::isBuildableWith(Type type) {
  return StencilDialect::isValidStencilScalarType(type);
}

//===----------------------------------------------------------------------===//
// stencil.field
//===----------------------------------------------------------------------===//

void stencil::FieldOp::build(Builder *builder, OperationState &state,
                             StringRef name) {
  // TODO: Handle other element types
  auto nameAttr = builder->getStringAttr(
      name, stencil::FieldType::get(builder->getF64Type()));
  state.addAttribute(kNameAttrName, nameAttr);
  state.addTypes(nameAttr.getType());
}

static ParseResult parseFieldOp(OpAsmParser &parser, OperationState &state) {
  StringAttr fieldName;
  if (parser.parseAttribute(fieldName, kNameAttrName, state.attributes))
    return failure();

  return parser.addTypesToList(fieldName.getType(), state.types);
}

static void print(stencil::FieldOp fieldOp, OpAsmPrinter &printer) {
  printer << stencil::FieldOp::getOperationName() << ' ';
  printer.printAttribute(fieldOp.getAttr(kNameAttrName));
}

static LogicalResult verify(stencil::FieldOp fieldOp) { return success(); }

//===----------------------------------------------------------------------===//
// stencil.field_access
//===----------------------------------------------------------------------===//

void stencil::FieldAccessOp::build(Builder *builder, OperationState &state,
                                   Value *field, ArrayRef<int64_t> offset) {
  assert(offset.size() == 3 && "offset expects three elements");
  state.addOperands(field);
  state.addAttribute(kOffsetAttrName, builder->getI64ArrayAttr(offset));
  state.addTypes(stencil::PointerType::get(
      field->getType().cast<stencil::FieldType>().getElementType()));
}

void stencil::FieldAccessOp::build(Builder *builder, OperationState &state,
                                   Value *field, ArrayAttr offsetAttr) {
  assert(offsetAttr.size() == 3 && "offset attribute expects three elements");
  state.addOperands(field);
  state.addAttribute(kOffsetAttrName, offsetAttr);
  state.addTypes(stencil::PointerType::get(
      field->getType().cast<stencil::FieldType>().getElementType()));
}

static ParseResult parseFieldAccessOp(OpAsmParser &parser,
                                      OperationState &state) {
  OpAsmParser::OperandType field;
  ArrayAttr offset;
  Type pointerType;
  if (parser.parseOperand(field) ||
      parser.parseAttribute(offset, kOffsetAttrName, state.attributes) ||
      parser.parseColonType(pointerType) ||
      parser.resolveOperand(
          field,
          stencil::FieldType::get(
              pointerType.cast<stencil::PointerType>().getPointeeType()),
          state.operands) ||
      parser.addTypesToList(pointerType, state.types))
    return failure();

  return success();
}

static void print(stencil::FieldAccessOp fieldAccessOp, OpAsmPrinter &printer) {
  printer << stencil::FieldAccessOp::getOperationName() << ' '
          << *fieldAccessOp.field() << ' ';
  printer.printAttribute(fieldAccessOp.getAttr(kOffsetAttrName));
  printer << " : " << fieldAccessOp.res()->getType();
}

// TODO: Check consistency
static LogicalResult verify(stencil::FieldAccessOp fieldAccessOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// stencil.var
//===----------------------------------------------------------------------===//

void stencil::VarOp::build(Builder *builder, OperationState &state,
                           StringRef name) {
  // TODO: Handle other element types
  auto nameAttr = builder->getStringAttr(
      name, stencil::VarType::get(builder->getF64Type()));
  state.addAttribute(kNameAttrName, nameAttr);
  state.addTypes(nameAttr.getType());
}

static ParseResult parseVarOp(OpAsmParser &parser, OperationState &state) {
  StringAttr varName;
  if (parser.parseAttribute(varName, kNameAttrName, state.attributes))
    return failure();

  return parser.addTypesToList(varName.getType(), state.types);
}

static void print(stencil::VarOp varOp, OpAsmPrinter &printer) {
  printer << stencil::VarOp::getOperationName() << ' ';
  printer.printAttribute(varOp.getAttr(kNameAttrName));
}

static LogicalResult verify(stencil::VarOp fieldOp) { return success(); }

//===----------------------------------------------------------------------===//
// stencil.var_access
//===----------------------------------------------------------------------===//

void stencil::VarAccessOp::build(Builder *builder, OperationState &state,
                                 Value *var) {
  state.addOperands(var);
  state.addTypes(stencil::PointerType::get(
      var->getType().cast<stencil::VarType>().getContentType()));
}

static ParseResult parseVarAccessOp(OpAsmParser &parser,
                                    OperationState &state) {
  OpAsmParser::OperandType var;
  Type contentType;
  if (parser.parseOperand(var) || parser.parseColonType(contentType) ||
      parser.resolveOperand(var, stencil::VarType::get(contentType),
                            state.operands) ||
      parser.addTypesToList(stencil::PointerType::get(contentType),
                            state.types))
    return failure();

  return success();
}

static void print(stencil::VarAccessOp varAccessOp, OpAsmPrinter &printer) {
  printer << stencil::VarAccessOp::getOperationName() << ' '
          << *varAccessOp.var() << " : "
          << varAccessOp.res()
                 ->getType()
                 .cast<stencil::PointerType>()
                 .getPointeeType();
}

// TODO: Check consistency
static LogicalResult verify(stencil::VarAccessOp varAccessOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// stencil.get_value
//===----------------------------------------------------------------------===//

void stencil::GetValueOp::build(Builder *builder, OperationState &state,
                                Value *ptr) {
  state.addOperands(ptr);
  state.addTypes(ptr->getType().cast<stencil::PointerType>().getPointeeType());
}

static ParseResult parseGetValueOp(OpAsmParser &parser, OperationState &state) {
  OpAsmParser::OperandType ptr;
  Type pointeeType;
  if (parser.parseOperand(ptr) || parser.parseColonType(pointeeType) ||
      parser.resolveOperand(ptr, stencil::PointerType::get(pointeeType),
                            state.operands) ||
      parser.addTypesToList(pointeeType, state.types))
    return failure();

  return success();
}

static void print(stencil::GetValueOp getValueOp, OpAsmPrinter &printer) {
  auto ptr = getValueOp.ptr();
  printer << stencil::GetValueOp::getOperationName() << ' ' << *ptr << " : "
          << ptr->getType().cast<stencil::PointerType>().getPointeeType();
}

static LogicalResult verify(stencil::GetValueOp getValueOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// stencil.write
//===----------------------------------------------------------------------===//

void stencil::WriteOp::build(Builder *builder, OperationState &state,
                             Value *ptr, Value *value) {
  state.addOperands({ptr, value});
}

static ParseResult parseWriteOp(OpAsmParser &parser, OperationState &state) {
  OpAsmParser::OperandType target, value;
  Type valueType;
  if (parser.parseOperand(target) || parser.parseComma() ||
      parser.parseOperand(value) || parser.parseColonType(valueType) ||
      parser.resolveOperands({target, value},
                             {stencil::PointerType::get(valueType), valueType},
                             parser.getCurrentLocation(), state.operands))
    return failure();

  return success();
}

static void print(stencil::WriteOp writeOp, OpAsmPrinter &printer) {
  auto target = writeOp.target();
  auto value = writeOp.value();
  printer << stencil::WriteOp::getOperationName() << ' ' << *target << ", "
          << *value << " : " << value->getType();
}

static LogicalResult verify(stencil::WriteOp writeOp) {
  Type targetType =
      writeOp.target()->getType().cast<stencil::PointerType>().getPointeeType();
  Type valueType = writeOp.value()->getType();

  if (targetType != valueType)
    return writeOp.emitOpError("inconsistent target type '")
           << targetType << "' and value type '" << valueType << "'";

  return success();
}

//===----------------------------------------------------------------------===//
// stencil.iir
//===----------------------------------------------------------------------===//

void stencil::IIROp::build(Builder *builder, OperationState &state) {
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// stencil.stencil
//===----------------------------------------------------------------------===//

void stencil::StencilOp::build(
    Builder *builder, OperationState &state,
    ArrayRef<stencil::StencilAttribute> stencilAttributes) {
  for (const auto &attr : stencilAttributes) {
    switch (attr) {
    case stencil::StencilAttribute::MergeDoMethods:
      state.addAttribute(kMergeDoMethodsAttrName, builder->getUnitAttr());
      break;
    case stencil::StencilAttribute::MergeStages:
      state.addAttribute(kMergeStagesAttrName, builder->getUnitAttr());
      break;
    case stencil::StencilAttribute::MergeTemporaries:
      state.addAttribute(kMergeTemporariesAttrName, builder->getUnitAttr());
      break;
    case stencil::StencilAttribute::NoCodeGen:
      state.addAttribute(kNoCodeGenAttrName, builder->getUnitAttr());
      break;
    case stencil::StencilAttribute::UseKCaches:
      state.addAttribute(kUseKCachesAttrName, builder->getUnitAttr());
      break;
    default:
      llvm_unreachable("Unknown stencil attribute");
    }
  }

  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static ParseResult parseStencilOp(OpAsmParser &parser, OperationState &state) {
  Region *bodyRegion = state.addRegion();
  SmallVector<OpAsmParser::OperandType, 3> arguments;
  SmallVector<Type, 3> argumentTypes;

  auto parseArgument = [&]() -> ParseResult {
    OpAsmParser::OperandType argument;
    Type argumentType;
    if (succeeded(parser.parseOptionalRegionArgument(argument)) &&
        !argument.name.empty()) {
      arguments.push_back(argument);

      if (parser.parseColonType(argumentType))
        return failure();
    } else {
      return failure();
    }

    argumentTypes.push_back(argumentType);
    return success();
  };

  if (parser.parseLParen())
    return failure();
  if (parser.parseOptionalRParen()) {
    do {
      if (parseArgument())
        return failure();
    } while (succeeded(parser.parseOptionalComma()));
    parser.parseRParen();
  }

  if (parser.parseRegion(*bodyRegion, arguments, argumentTypes))
    return failure();

  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();

  stencil::StencilOp::ensureTerminator(*bodyRegion, parser.getBuilder(),
                                       state.location);

  return success();
}

static void print(stencil::StencilOp stencilOp, OpAsmPrinter &printer) {
  auto *op = stencilOp.getOperation();
  auto &body = stencilOp.getBody();

  printer << stencilOp.getOperationName() << '(';
  for (unsigned i = 0, e = body.getNumArguments(); i < e; ++i) {
    if (i > 0)
      printer << ", ";

    auto arg = body.getArgument(i);

    printer.printOperand(arg);
    printer << ": ";
    printer.printType(arg->getType());
  }
  printer << ')';

  printer.printRegion(op->getRegion(0), /*printEntryBlockArgs=*/false,
                      /*printBlockTerminators=*/false);
}

//===----------------------------------------------------------------------===//
// stencil.multi_stage
//===----------------------------------------------------------------------===//

void stencil::MultiStageOp::build(Builder *builder, OperationState &state,
                                  stencil::LoopOrder loopOrder) {
  state.addAttribute(kLoopOrderAttrName, builder->getI64IntegerAttr(
                                             static_cast<int64_t>(loopOrder)));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static ParseResult parseLoopOrder(stencil::LoopOrder &value,
                                  OpAsmParser &parser, OperationState &state) {
  Attribute attrVal;
  SmallVector<NamedAttribute, 1> attr;
  auto loc = parser.getCurrentLocation();
  if (parser.parseAttribute(attrVal, parser.getBuilder().getNoneType(),
                            "loopOrder", attr)) {
    return failure();
  }
  if (!attrVal.isa<StringAttr>()) {
    return parser.emitError(loc, "expected ")
           << "loopOrder attribute specified as string";
  }
  auto attrOptional =
      stencil::symbolizeLoopOrder(attrVal.cast<StringAttr>().getValue());
  if (!attrOptional) {
    return parser.emitError(loc, "invalid ")
           << "loopOrder attribute specification: " << attrVal;
  }
  value = attrOptional.getValue();

  state.addAttribute(kLoopOrderAttrName, parser.getBuilder().getI64IntegerAttr(
                                             bitwiseCast<int64_t>(value)));

  return success();
}

static ParseResult parseMultiStageOp(OpAsmParser &parser,
                                     OperationState &state) {
  stencil::LoopOrder loopOrderAttr;
  if (parseLoopOrder(loopOrderAttr, parser, state))
    return failure();

  Region *body = state.addRegion();
  if (parser.parseRegion(*body, /*arguments=*/{}, /*argTypes=*/{}))
    return failure();

  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();

  stencil::MultiStageOp::ensureTerminator(*body, parser.getBuilder(),
                                          state.location);

  return success();
}

static void print(stencil::MultiStageOp multiStageOp, OpAsmPrinter &printer) {
  auto *op = multiStageOp.getOperation();

  printer << multiStageOp.getOperationName() << " \""
          << stencil::stringifyLoopOrder(multiStageOp.loopOrder()) << "\"";

  printer.printRegion(op->getRegion(0), /*printEntryBlockArgs=*/false,
                      /*printBlockTerminators=*/false);

  if (op->getAttrs().size() > 1) {
    printer << " attributes";
    printer.printOptionalAttrDict(op->getAttrs(), {kLoopOrderAttrName});
  }
}

//===----------------------------------------------------------------------===//
// stencil.stage
//===----------------------------------------------------------------------===//

void stencil::StageOp::build(Builder *builder, OperationState &state) {
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// stencil.do_method
//===----------------------------------------------------------------------===//

void stencil::DoMethodOp::build(Builder *builder, OperationState &state,
                                ArrayRef<int64_t> interval) {
  assert(interval.size() == 4 && "interval must be built using four integers");

  llvm::SmallVector<NamedAttribute, 4> intervalAttr = {
      {builder->getIdentifier(kIntervalLowerLevelAttrName),
       builder->getI64IntegerAttr(interval[0])},
      {builder->getIdentifier(kIntervalUpperLevelAttrName),
       builder->getI64IntegerAttr(interval[1])},
      {builder->getIdentifier(kIntervalLowerOffsetAttrName),
       builder->getI64IntegerAttr(interval[2])},
      {builder->getIdentifier(kIntervalUpperOffsetAttrName),
       builder->getI64IntegerAttr(interval[3])}};

  state.addAttribute(kIntervalAttrName,
                     builder->getDictionaryAttr(intervalAttr));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static ParseResult parseDoMethodOp(OpAsmParser &parser, OperationState &state) {
  IntegerAttr lowerLevel, lowerOffset, upperLevel, upperOffset;
  SmallVector<NamedAttribute, 4> intervalAttr;
  if (parser.parseLSquare() ||
      parser.parseAttribute(lowerLevel, "lowerLevel", intervalAttr) ||
      parser.parseComma() ||
      parser.parseAttribute(lowerOffset, "lowerOffset", intervalAttr) ||
      parser.parseComma() ||
      parser.parseAttribute(upperLevel, "upperLevel", intervalAttr) ||
      parser.parseComma() ||
      parser.parseAttribute(upperOffset, "upperOffset", intervalAttr) ||
      parser.parseRSquare())
    return failure();
  state.addAttribute("interval",
                     parser.getBuilder().getDictionaryAttr(intervalAttr));

  Region *body = state.addRegion();
  if (parser.parseRegion(*body, /*arguments=*/{}, /*argTypes=*/{}))
    return failure();

  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();

  stencil::DoMethodOp::ensureTerminator(*body, parser.getBuilder(),
                                        state.location);

  return success();
}

static void print(stencil::DoMethodOp doMethodOp, OpAsmPrinter &printer) {
  auto *op = doMethodOp.getOperation();
  auto intervalAttr = doMethodOp.interval();
  auto lowerLevel = intervalAttr.get("lowerLevel").cast<IntegerAttr>().getInt();
  auto lowerOffset =
      intervalAttr.get("lowerOffset").cast<IntegerAttr>().getInt();
  auto upperLevel = intervalAttr.get("upperLevel").cast<IntegerAttr>().getInt();
  auto upperOffset =
      intervalAttr.get("upperOffset").cast<IntegerAttr>().getInt();

  printer << doMethodOp.getOperationName() << " [" << lowerLevel << ", "
          << lowerOffset << ", " << upperLevel << ", " << upperOffset << "]";

  printer.printRegion(op->getRegion(0), /*printEntryBlockArgs=*/false,
                      /*printBlockTerminators=*/false);

  if (op->getAttrs().size() > 1) {
    printer << " attributes";
    printer.printOptionalAttrDict(op->getAttrs(), {kLoopOrderAttrName});
  }
}

namespace mlir {
namespace stencil {
#define GET_OP_CLASSES
#include "mlir/Dialect/Stencil/Stencil/StencilOps.cpp.inc"
} // namespace stencil
} // namespace mlir
