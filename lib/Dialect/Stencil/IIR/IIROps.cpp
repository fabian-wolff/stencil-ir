//===- IIROps.cpp - MLIR stencil IIR operations ---------------------------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file defines the operations in the stencil IIR dialect.
//
//===----------------------------------------------------------------------===//

#include "mlir/Dialect/Stencil/IIR/IIROps.h"

#include "mlir/Dialect/Stencil/IIR/IIRDialect.h"
#include "mlir/Dialect/Stencil/IIR/IIRTypes.h"
#include "mlir/IR/Attributes.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Function.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/StandardTypes.h"

using namespace mlir;

static constexpr const char kIDAttrName[] = "id";
static constexpr const char kValueAttrName[] = "value";
static constexpr const char kMergeDoMethodsAttrName[] = "mergeDoMethods";
static constexpr const char kMergeStagesAttrName[] = "mergeStages";
static constexpr const char kMergeTemporariesAttrName[] = "mergeTemporaries";
static constexpr const char kNoCodeGenAttrName[] = "noCodeGen";
static constexpr const char kUseKCachesAttrName[] = "useKCaches";
static constexpr const char kLoopOrderAttrName[] = "loopOrder";
static constexpr const char kIntervalAttrName[] = "interval";
static constexpr const char kIntervalLowerLevelAttrName[] = "lowerLevel";
static constexpr const char kIntervalUpperLevelAttrName[] = "upperLevel";
static constexpr const char kIntervalLowerOffsetAttrName[] = "lowerOffset";
static constexpr const char kIntervalUpperOffsetAttrName[] = "upperOffset";
static constexpr const char kOperatorAttrName[] = "op";
static constexpr const char kNameAttrName[] = "name";
static constexpr const char kOffsetAttrName[] = "offset";
static constexpr const char kArgumentMapAttrName[] = "argumentMap";
static constexpr const char kArgumentOffsetAttrName[] = "argumentOffset";
static constexpr const char kNegateOffsetAttrName[] = "negateOffset";
static constexpr const char kReadAccessesAttrName[] = "readAccesses";
static constexpr const char kWriteAccessesAttrName[] = "writeAccesses";
static constexpr const char kIsExternalAttrName[] = "isExternal";
static constexpr const char kBuiltinTypeAttrName[] = "builtinType";
static constexpr const char kCalleeAttrName[] = "callee";
static constexpr const char kIndexAttrName[] = "index";
static constexpr const char kDimensionAttrName[] = "dimension";
static constexpr const char kArgumentsAttrName[] = "arguments";

//===----------------------------------------------------------------------===//
// Common utility functions
//===----------------------------------------------------------------------===//

template <typename Dst, typename Src>
inline Dst bitwiseCast(Src source) noexcept {
  Dst dest;
  static_assert(sizeof(source) == sizeof(dest),
                "bitwiseCast requires same source and destination bitwidth");
  std::memcpy(&dest, &source, sizeof(dest));
  return dest;
}

// Parses an op that has no inputs and no outputs.
static ParseResult parseNoIOOp(OpAsmParser &parser, OperationState &state) {
  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();
  return success();
}

// Prints an op that has no inputs and no outputs.
static void printNoIOOp(Operation *op, OpAsmPrinter &printer) {
  printer << op->getName();
  printer.printOptionalAttrDict(op->getAttrs());
}

static void printRegionOp(Operation *op, OpAsmPrinter &printer) {
  printer << op->getName();

  for (auto &region : op->getRegions()) {
    printer.printRegion(region, false, false);
  }

  if (!op->getAttrs().empty()) {
    printer << " attributes";
    printer.printOptionalAttrDict(op->getAttrs());
  }
}

template <typename StructureOp>
static ParseResult parseRegionOp(OpAsmParser &parser, OperationState &state,
                                 unsigned int nRegions = 1) {
  llvm::SmallVector<Region *, 2> regions;
  for (unsigned int i = 0; i < nRegions; ++i)
    regions.push_back(state.addRegion());

  for (auto &region : regions) {
    if (parser.parseRegion(*region, /*arguments=*/{}, /*argTypes=*/{}))
      return failure();
    StructureOp::ensureTerminator(*region, parser.getBuilder(), state.location);
  }

  if (succeeded(parser.parseOptionalKeyword("attributes"))) {
    if (parser.parseOptionalAttributeDict(state.attributes))
      return failure();
  }

  return success();
}

template <typename Op>
static LogicalResult verifyIIRBody(Op op) {
  auto *dialect = op.getOperation()->getDialect();

  for (auto &region : op.getOperation()->getRegions()) {
    for (auto &block : region) {
      for (auto &operation : block) {
        if (operation.getDialect() != dialect)
          return operation.emitError(std::string("'") + op.getOperationName() +
                                     "' can only contain iir.* ops");
      }
    }
  }

  return success();
}

//===----------------------------------------------------------------------===//
// iir.iir
//===----------------------------------------------------------------------===//

void iir::IIROp::build(Builder *builder, OperationState &state) {
  // Stencils
  ensureTerminator(*state.addRegion(), *builder, state.location);
  // ID to stencil call
  ensureTerminator(*state.addRegion(), *builder, state.location);
  // Control flow
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.stencil
//===----------------------------------------------------------------------===//

void iir::StencilOp::build(Builder *builder, OperationState &state, int64_t id,
                           ArrayRef<iir::StencilAttribute> stencilAttributes) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  for (const auto &attr : stencilAttributes) {
    switch (attr) {
    case iir::StencilAttribute::MergeDoMethods:
      state.addAttribute(kMergeDoMethodsAttrName, builder->getUnitAttr());
      break;
    case iir::StencilAttribute::MergeStages:
      state.addAttribute(kMergeStagesAttrName, builder->getUnitAttr());
      break;
    case iir::StencilAttribute::MergeTemporaries:
      state.addAttribute(kMergeTemporariesAttrName, builder->getUnitAttr());
      break;
    case iir::StencilAttribute::NoCodeGen:
      state.addAttribute(kNoCodeGenAttrName, builder->getUnitAttr());
      break;
    case iir::StencilAttribute::UseKCaches:
      state.addAttribute(kUseKCachesAttrName, builder->getUnitAttr());
      break;
    default:
      llvm_unreachable("Unknown stencil attribute");
    }
  }
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static LogicalResult verify(iir::StencilOp stencilsOp) {
  auto &body = stencilsOp.getBody();

  for (auto &op : body) {
    if (!isa<iir::StencilEndOp>(op) && !isa<iir::MultiStageOp>(op))
      return op.emitError(
          "'iir.stencil' can only contain 'iir.multi_stage' ops");
  }

  return success();
}

//===----------------------------------------------------------------------===//
// iir.multi_stage
//===----------------------------------------------------------------------===//

void iir::MultiStageOp::build(Builder *builder, OperationState &state,
                              int64_t id, iir::LoopOrder loopOrder) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kLoopOrderAttrName, builder->getI64IntegerAttr(
                                             static_cast<int64_t>(loopOrder)));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static LogicalResult verify(iir::MultiStageOp multiStageOp) {
  auto &body = multiStageOp.getBody();

  for (auto &op : body) {
    if (!isa<iir::MultiStageEndOp>(op) && !isa<iir::StageOp>(op))
      return op.emitError("'iir.multi_stage' can only contain 'iir.stage' ops");
  }

  return success();
}

//===----------------------------------------------------------------------===//
// iir.stage
//===----------------------------------------------------------------------===//

void iir::StageOp::build(Builder *builder, OperationState &state, int64_t id) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static LogicalResult verify(iir::StageOp stageOp) {
  auto &body = stageOp.getBody();

  for (auto &op : body) {
    if (!isa<iir::StageEndOp>(op) && !isa<iir::DoMethodOp>(op))
      return op.emitError("'iir.stage' can only contain 'iir.do_method' ops");
  }

  return success();
}

//===----------------------------------------------------------------------===//
// iir.do_method
//===----------------------------------------------------------------------===//

void iir::DoMethodOp::build(Builder *builder, OperationState &state, int64_t id,
                            ArrayRef<int64_t> interval) {
  assert(interval.size() == 4 && "interval must be built using four integers");

  llvm::SmallVector<NamedAttribute, 4> intervalAttr = {
      {builder->getIdentifier(kIntervalLowerLevelAttrName),
       builder->getI64IntegerAttr(interval[0])},
      {builder->getIdentifier(kIntervalUpperLevelAttrName),
       builder->getI64IntegerAttr(interval[1])},
      {builder->getIdentifier(kIntervalLowerOffsetAttrName),
       builder->getI64IntegerAttr(interval[2])},
      {builder->getIdentifier(kIntervalUpperOffsetAttrName),
       builder->getI64IntegerAttr(interval[3])}};

  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kIntervalAttrName,
                     builder->getDictionaryAttr(intervalAttr));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

static LogicalResult verify(iir::DoMethodOp doMethodOp) {
  auto &body = doMethodOp.getBody();

  for (auto &op : body) {
    if (!isa<iir::DoMethodEndOp>(op) && !isa<iir::StatementAccessPairOp>(op))
      return op.emitError(
          "'iir.do_method' can only contain 'iir.statement_access_pair' ops");
  }

  return success();
}

//===----------------------------------------------------------------------===//
// iir.statement_access_pair
//===----------------------------------------------------------------------===//

// Note: writeAccesses and readAccesses are expected to be of the form
// [id0, iminus0, iplus0, jminus0, jplus0, kminus0, kplus0,
//  id1, iminus1, ...]
// i.e. there are flattened versions of the writeAccess and readAccess maps
void iir::StatementAccessPairOp::build(Builder *builder, OperationState &state,
                                       ArrayRef<int64_t> writeAccesses,
                                       ArrayRef<int64_t> readAccesses) {
  assert(writeAccesses.size() % 7 == 0 &&
         "'writeAccesses' is expected to be of the form [id0, iminus0, iplus0, "
         "..., kplus0, id1, iminus1, ...]");
  assert(readAccesses.size() % 7 == 0 &&
         "'readAccesses' is expected to be of the form [id0, iminus0, iplus0, "
         "..., kplus0, id1, iminus1, ...]");
  state.addAttribute(kWriteAccessesAttrName,
                     builder->getI64ArrayAttr(writeAccesses));
  state.addAttribute(kReadAccessesAttrName,
                     builder->getI64ArrayAttr(readAccesses));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.expr_statement
//===----------------------------------------------------------------------===//

void iir::ExprStatementOp::build(Builder *builder, OperationState &state,
                                 int64_t id) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.assignment_expr
//===----------------------------------------------------------------------===//

void iir::AssignmentExprOp::build(Builder *builder, OperationState &state,
                                  int64_t id, iir::AssignmentOperator op) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kOperatorAttrName,
                     builder->getI64IntegerAttr(static_cast<int64_t>(op)));
  // Left
  ensureTerminator(*state.addRegion(), *builder, state.location);
  // Right
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.field_access_expr
//===----------------------------------------------------------------------===//

void iir::FieldAccessExprOp::build(Builder *builder, OperationState &state,
                                   int64_t id, StringRef name,
                                   ArrayRef<int64_t> offset,
                                   ArrayRef<int64_t> argumentMap,
                                   ArrayRef<int64_t> argumentOffset,
                                   bool negateOffset) {
  assert(offset.size() == 3 && "offset must contain three integers");
  assert(argumentMap.size() == 3 && "argumentMap must contain three integers");
  assert(argumentOffset.size() == 3 &&
         "argumentOffset must contain three integers");

  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kNameAttrName, builder->getStringAttr(name));
  state.addAttribute(kOffsetAttrName, builder->getI64ArrayAttr(offset));
  state.addAttribute(kArgumentMapAttrName,
                     builder->getI64ArrayAttr(argumentMap));
  state.addAttribute(kArgumentOffsetAttrName,
                     builder->getI64ArrayAttr(argumentOffset));
  state.addAttribute(kNegateOffsetAttrName, builder->getBoolAttr(negateOffset));
}

static ParseResult parseFieldAccessExprOp(OpAsmParser &parser,
                                          OperationState &state) {
  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();

  return success();
}

static void print(iir::FieldAccessExprOp fieldAccessExprOp,
                  OpAsmPrinter &printer) {
  auto *op = fieldAccessExprOp.getOperation();

  printer << fieldAccessExprOp.getOperationName();

  if (!op->getAttrs().empty()) {
    printer.printOptionalAttrDict(op->getAttrs());
  }
}

static LogicalResult verify(iir::FieldAccessExprOp fieldAccessExprOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// iir.var_decl_statement
//===----------------------------------------------------------------------===//

void iir::VarDeclStatementOp::build(Builder *builder, OperationState &state,
                                    int64_t id) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.binary_operator
//===----------------------------------------------------------------------===//

void iir::BinaryOperatorOp::build(Builder *builder, OperationState &state,
                                  int64_t id, iir::BinaryOperator op) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kOperatorAttrName,
                     builder->getI64IntegerAttr(static_cast<int64_t>(op)));
  // Left
  ensureTerminator(*state.addRegion(), *builder, state.location);
  // Right
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.var_access_expr
//===----------------------------------------------------------------------===//

void iir::VarAccessExprOp::build(Builder *builder, OperationState &state,
                                 int64_t id, StringRef name, bool isExternal) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kNameAttrName, builder->getStringAttr(name));
  state.addAttribute(kIsExternalAttrName, builder->getBoolAttr(isExternal));
  // Index
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.literal_access_expr
//===----------------------------------------------------------------------===//

void iir::LiteralAccessExprOp::build(Builder *builder, OperationState &state,
                                     int64_t id, StringRef value,
                                     iir::BuiltinType builtinType) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kValueAttrName, builder->getStringAttr(value));
  state.addAttribute(
      kBuiltinTypeAttrName,
      builder->getI64IntegerAttr(static_cast<int64_t>(builtinType)));
}

static ParseResult parseLiteralAccessExprOp(OpAsmParser &parser,
                                            OperationState &state) {
  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();

  return success();
}

static void print(iir::LiteralAccessExprOp literalAccessExprOp,
                  OpAsmPrinter &printer) {
  auto *op = literalAccessExprOp.getOperation();

  printer << literalAccessExprOp.getOperationName();

  if (!op->getAttrs().empty()) {
    printer.printOptionalAttrDict(op->getAttrs());
  }
}

static LogicalResult verify(iir::LiteralAccessExprOp literalAccessExprOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// iir.ternary_operator
//===----------------------------------------------------------------------===//

void iir::TernaryOperatorOp::build(Builder *builder, OperationState &state,
                                   int64_t id) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  // Cond
  ensureTerminator(*state.addRegion(), *builder, state.location);
  // Left
  ensureTerminator(*state.addRegion(), *builder, state.location);
  // Right
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.unary_operator
//===----------------------------------------------------------------------===//

void iir::UnaryOperatorOp::build(Builder *builder, OperationState &state,
                                 int64_t id, iir::UnaryOperator op) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kOperatorAttrName,
                     builder->getI64IntegerAttr(static_cast<int64_t>(op)));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.function_call
//===----------------------------------------------------------------------===//

void iir::FunCallOp::build(Builder *builder, OperationState &state, int64_t id,
                           StringRef callee) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kCalleeAttrName, builder->getStringAttr(callee));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.stencil_function_call
//===----------------------------------------------------------------------===//

void iir::StencilFunCallOp::build(Builder *builder, OperationState &state,
                                  int64_t id, StringRef callee) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kCalleeAttrName, builder->getStringAttr(callee));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.stencil_function_argument_expr
//===----------------------------------------------------------------------===//

void iir::StencilFunArgExprOp::build(Builder *builder, OperationState &state,
                                     int64_t id, int64_t index, int64_t offset,
                                     iir::Dimension dimension) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kIndexAttrName, builder->getI64IntegerAttr(index));
  state.addAttribute(kOffsetAttrName, builder->getI64ArrayAttr(offset));
  state.addAttribute(kDimensionAttrName, builder->getI64IntegerAttr(
                                             static_cast<int64_t>(dimension)));
}

void iir::StencilFunArgExprOp::build(Builder *builder, OperationState &state,
                                     int64_t id, int64_t index,
                                     int64_t offset) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  state.addAttribute(kIndexAttrName, builder->getI64IntegerAttr(index));
  state.addAttribute(kOffsetAttrName, builder->getI64ArrayAttr(offset));
}

static ParseResult parseStencilFunArgExprOp(OpAsmParser &parser,
                                            OperationState &state) {
  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();

  return success();
}

static void print(iir::StencilFunArgExprOp stencilFunArgExprOp,
                  OpAsmPrinter &printer) {
  auto *op = stencilFunArgExprOp.getOperation();
  printer << stencilFunArgExprOp.getOperationName();
  printer.printOptionalAttrDict(op->getAttrs());
}

static LogicalResult verify(iir::StencilFunArgExprOp stencilFunArgExprOp) {
  return success();
}

//===----------------------------------------------------------------------===//
// iir.if
//===----------------------------------------------------------------------===//

void iir::IfOp::build(Builder *builder, OperationState &state, int64_t id) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  // Cond
  ensureTerminator(*state.addRegion(), *builder, state.location);
  // Then
  ensureTerminator(*state.addRegion(), *builder, state.location);
  // Else
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.block_statement
//===----------------------------------------------------------------------===//

void iir::BlockStatementOp::build(Builder *builder, OperationState &state,
                                  int64_t id) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.id_to_stencil_call_entry
//===----------------------------------------------------------------------===//

void iir::IDToStencilCallEntryOp::build(Builder *builder, OperationState &state,
                                        int64_t id) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.stencil_call_decl_statement
//===----------------------------------------------------------------------===//

void iir::StencilCallDeclStatementOp::build(Builder *builder,
                                            OperationState &state, int64_t id) {
  state.addAttribute(kIDAttrName, builder->getI64IntegerAttr(id));
  ensureTerminator(*state.addRegion(), *builder, state.location);
}

//===----------------------------------------------------------------------===//
// iir.stencil_call
//===----------------------------------------------------------------------===//

void iir::StencilCallOp::build(Builder *builder, OperationState &state,
                               StringRef callee,
                               ArrayRef<StringRef> arguments) {
  state.addAttribute(kCalleeAttrName, builder->getStringAttr(callee));
  state.addAttribute(kArgumentsAttrName, builder->getStrArrayAttr(arguments));
}

static ParseResult parseStencilCallOp(OpAsmParser &parser,
                                      OperationState &state) {
  if (parser.parseOptionalAttributeDict(state.attributes))
    return failure();

  return success();
}

static void print(iir::StencilCallOp stencilCallOp, OpAsmPrinter &printer) {
  auto *op = stencilCallOp.getOperation();

  printer << stencilCallOp.getOperationName();

  if (!op->getAttrs().empty()) {
    printer.printOptionalAttrDict(op->getAttrs());
  }
}

static LogicalResult verify(iir::StencilCallOp stencilCallOp) {
  return success();
}

namespace mlir {
namespace iir {
#define GET_OP_CLASSES
#include "mlir/Dialect/Stencil/IIR/IIROps.cpp.inc"
} // namespace iir
} // namespace mlir
