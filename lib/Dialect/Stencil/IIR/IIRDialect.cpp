//===- IIRDialect.cpp - MLIR stencil IIR dialect --------------------------===//
//
// Copyright 2019 Jean-Michel Gorius
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================
//
// This file defines the stencil IIR dialect in MLIR.
//
//===----------------------------------------------------------------------===//

#include "mlir/Dialect/Stencil/IIR/IIRDialect.h"
#include "mlir/Dialect/Stencil/IIR/IIROps.h"
#include "mlir/Dialect/Stencil/IIR/IIRTypes.h"
#include "mlir/IR/Builders.h"

using namespace mlir;
using namespace mlir::iir;

//===----------------------------------------------------------------------===//
// Stencil IIR Dialect
//===----------------------------------------------------------------------===//

IIRDialect::IIRDialect(mlir::MLIRContext *context)
    : Dialect(getDialectNamespace(), context) {
  // addTypes<>();

  addOperations<
#define GET_OP_LIST
#include "mlir/Dialect/Stencil/IIR/IIROps.cpp.inc"
      >();

  allowsUnknownOperations();
}

//===----------------------------------------------------------------------===//
// Type Parsing
//===----------------------------------------------------------------------===//

Type IIRDialect::parseType(StringRef spec, Location loc) const {
  emitError(loc, "unknown IIR type: ") << spec;
  return Type();
}

//===----------------------------------------------------------------------===//
// Type Printing
//===----------------------------------------------------------------------===//

void IIRDialect::printType(Type type, raw_ostream &os) const {
  switch (type.getKind()) {
  default:
    llvm_unreachable("unhandled IIR type");
  }
}
